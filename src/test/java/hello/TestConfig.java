package hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {
    @Bean
    public TestEnv testEnv(@Value("${app.env:dev}") String env) {
        return new TestEnv(env);
    }
}
