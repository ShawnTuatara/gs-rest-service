package hello;

public class TestEnv {
    private String env;

    public TestEnv(String env) {
        this.env = env;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
